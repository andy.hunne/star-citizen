Star Citizen CH Products profile

This is my personal profile for my CH Products setup, consisting of:
- FighterStick
- ProThrottle 
- ProPedals

My FighterStick and ProThrottle have been customised for left-handed use, so some buttons/hats are reversed. You may need to re-map commands from the CMC file to suit your preference if you're a righty :).

Some of the functions (mouse positioning) in this profile rely on loading the profile via the Control Manager Control Centre tool - located in your CH Products install folder.  Check the CMS file for some further comments on how to do this.

You should clear all Joystick/Hotas mappings in Star Citizen EXCEPT the control axes you'll use with your physical devices (pitch, yay, roll, throttle, etc.). I have the mining laser power control mapped to the Z-wheel on the FighterStick.  All other buttons should be removed.

The triggers in the profile do not act as DirectX buttons. The rationale here is to map them to key commands in order to allow for up to 4 modes of operation.  Right now this isn't too complicated, but as CIG add more functions in future, it leaves room for flexibility.

You should take a look at the CMC file for the keymaps you'll need to change in the Keyboard/Mouse configuration section of Star Citizen.  I've tried to maintain the defaults where possible. About 90% of the commands are available in the CMC file - feel free to add more if they are of use.
